import unittest

roman_map = [(1000, 'M'), (900, 'CM'), (500, 'D'), (400, 'CD'), (100, 'C'), (90, 'XC'),
             (50, 'L'), (40, 'XL'), (10, 'X'), (9, 'IX'), (5, 'V'), (4, 'IV'), (1, 'I')]
arabic_map = {'M': 1000, 'CM': 900, 'D': 500, 'CD': 400, 'C': 100, 'XC': 90,
              'L': 50, 'XL': 40, 'X': 10, 'IX': 9, 'V': 5, 'IV': 4, 'I': 1}
keys = ['CM', 'M', 'CD', 'D', 'XC', 'C', 'XL', 'L', 'IX', 'X', 'IV', 'V', 'I']


def to_roman(number):
    if number <= 0 or number > 3999:
        raise ValueError('Provided number {0} is not allowed'.format(number))
    result = ''

    for key, sign in roman_map:
        while number >= key:
            number -= key
            result += sign

    return result


def to_arabic(number):
    check_roman_value(number)
    result = 0
    number = number.upper()
    for key in keys:
        if key in number:
            result += number.count(key) * arabic_map[key]
            number = number.replace(key, '')

    return result


def check_roman_value(number):
    invalid_keys = ['IIII', 'VV', 'XXXX', 'LL', 'CCCC', 'DD', 'MMMM']

    if any(invalid_key in number for invalid_key in invalid_keys):
        raise ValueError('Provided roman number {0} is not allowed'.format(number))


# for invalid_key in invalid_keys


def convert(value):
    result = None
    try:
        result = to_roman(value) if isinstance(value, int) else to_arabic(value)
    except ValueError as error:
        return '{0}'.format(error)
    else:
        return result


class TestRomanConversion(unittest.TestCase):
    def setUp(self):
        self.values = [(2479, 'MMCDLXXIX'), (44, 'XLIV'), (6, 'VI'), (3, 'III')]

    def test_to_roman(self):
        for arabic, roman in self.values:
            self.assertEqual(arabic, convert(roman))

    def test_to_arabic(self):
        for roman, arabic in self.values:
            self.assertEqual(roman, convert(arabic))

    def test_invalid_value(self):
        self.assertEqual('Provided number -3 is not allowed', convert(-3))
        self.assertEqual('Provided roman number MMVVV is not allowed', convert('MMVVV'))


if __name__ == '__main__':
    unittest.main()
